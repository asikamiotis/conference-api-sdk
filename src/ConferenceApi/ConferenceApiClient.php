<?php

namespace ConferenceApi;

use GuzzleHttp\Client;

class ConferenceApiClient
{
    protected $client;
    protected $headers;

    /**
     * @param string $apiKey
     * @param string $host
     */
    function __construct(string $apiKey, string $host)
    {
        $this->client = new Client([
            'base_uri' => $host
        ]);

        $this->headers = [
            'Authorization' => 'Bearer ' . $apiKey,
            'Accept' => 'application/json',
            'content-type' => 'application/json'
        ];
    }

    /**
     * ROOMS
     */

    /**
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAllRooms()
    {
        return $this->client->request('GET', 'rooms', [
            'headers' => $this->headers
        ]);
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getRoom(string $roomId)
    {
        return $this->client->request('GET', 'rooms/' . $roomId, [
            'headers' => $this->headers
        ]);
    }

    /**
     * @param array $parameters
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createRoom(array $parameters)
    {
        return $this->client->request('POST', 'rooms', [
            'headers' => $this->headers,
            'json' => $parameters
        ]);
    }

    /**
     * @param string $roomId
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function deleteRoom(string $roomId)
    {
        return $this->client->request('DELETE', 'rooms/' . $roomId, [
            'headers' => $this->headers
        ]);
    }

    /**
     * SERIES
     */

    /**
     * @param array $parameters
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createSeries(array $parameters)
    {
        return $this->client->request('POST', 'rooms', [
            'headers' => $this->headers,
            'json' => $parameters
        ]);
    }

    /**
     * @param string $seriesId
     * @param array $parameters
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addSeriesParticipants(string $seriesId, array $parameters)
    {
        return $this->client->request('POST', 'series/' . $seriesId . '/participants', [
            'headers' => $this->headers,
            'json' => $parameters
        ]);
    }

    /**
     * @param string $seriesId
     * @param string $participantIdentifier
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function removeSeriesParticipant(string $seriesId, string $participantIdentifier)
    {
        return $this->client->request('DELETE', 'series/' . $seriesId . '/participants/' . $participantIdentifier, [
            'headers' => $this->headers
        ]);
    }

    /**
     * MEETINGS
     */

    /**
     * @param array $parameters
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createMeeting(array $parameters)
    {
        return $this->client->request('POST', 'meetings', [
            'headers' => $this->headers,
            'json' => $parameters
        ]);
    }

    /**
     * @param string $meetingId
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getMeeting(string $meetingId)
    {
        return $this->client->request('GET', 'meetings/' . $meetingId, [
            'headers' => $this->headers
        ]);
    }

    /**
     * @param string $meetingId
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getMeetingReport(string $meetingId)
    {
        return $this->client->request('GET', 'meetings/' . $meetingId . '/report', [
            'headers' => $this->headers
        ]);
    }

    /**
     * @param string $meetingId
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getMeetingRecordings(string $meetingId)
    {
        return $this->client->request('GET', 'meetings/' . $meetingId . '/recordings', [
            'headers' => $this->headers
        ]);
    }

    /**
     * @param string $meetingId
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function startMeeting(string $meetingId)
    {
        return $this->client->request('PUT', 'meetings/' . $meetingId . '/start', [
            'headers' => $this->headers
        ]);
    }

    /**
     * @param string $meetingId
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function endMeeting(string $meetingId)
    {
        return $this->client->request('PUT', 'meetings/' . $meetingId . '/end', [
            'headers' => $this->headers
        ]);
    }

    /**
     * @param string $meetingId
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function lockMeeting(string $meetingId)
    {
        return $this->client->request('PUT', 'meetings/' . $meetingId . '/lock', [
            'headers' => $this->headers
        ]);
    }

    /**
     * @param string $meetingId
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function unlockMeeting(string $meetingId)
    {
        return $this->client->request('PUT', 'meetings/' . $meetingId . '/unlock', [
            'headers' => $this->headers
        ]);
    }

    /**
     * @param string $meetingId
     * @param array $parameters
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateMeeting(string $meetingId, array $parameters)
    {
        return $this->client->request('PATCH', 'meetings/' . $meetingId, [
            'headers' => $this->headers,
            'json' => $parameters
        ]);
    }

    /**
     * @param string $meetingId
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function deleteMeeting(string $meetingId)
    {
        return $this->client->request('DELETE', 'meetings/' . $meetingId, [
            'headers' => $this->headers
        ]);
    }

    /**
     * @param string $meetingId
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getMeetingParticipants(string $meetingId)
    {
        return $this->client->request('GET', 'meetings/' . $meetingId . '/participants', [
            'headers' => $this->headers
        ]);
    }

    /**
     * @param string $meetingId
     * @param array $parameters
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateParticipantsStatus(string $meetingId, array $parameters)
    {
        return $this->client->request('PATCH', 'meetings/' . $meetingId . '/participants/status', [
            'headers' => $this->headers,
            'json' => $parameters
        ]);
    }

     /**
     * @param string $meetingId
     * @param array $parameters
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateParticipantsH323Address(string $meetingId, array $parameters)
    {
        return $this->client->request('PATCH', 'meetings/' . $meetingId . '/participants/h323_address', [
            'headers' => $this->headers,
            'json' => $parameters
        ]);
    }

    /**
     * @param string $meetingId
     * @param array $parameters
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addMeetingParticipants(string $meetingId, array $parameters)
    {
        return $this->client->request('POST', 'meetings/' . $meetingId . '/participants', [
            'headers' => $this->headers,
            'json' => $parameters
        ]);
    }

    /**
     * @param string $meetingId
     * @param string $participantIdentifier
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function removeMeetingParticipant(string $meetingId, string $participantIdentifier)
    {
        return $this->client->request('DELETE', 'meetings/' . $meetingId . '/participants/' . $participantIdentifier, [
            'headers' => $this->headers
        ]);
    }

    /**
     * @param string $meetingId
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getPolls(string $meetingId)
    {
        return $this->client->request('GET', 'meetings/' . $meetingId . '/polls', [
            'headers' => $this->headers
        ]);
    }

    /**
     * @param string $meetingId
     * @param string $pollId
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getPoll(string $meetingId, string $pollId)
    {
        return $this->client->request('GET', 'meetings/' . $meetingId . '/polls/' . $pollId, [
            'headers' => $this->headers
        ]);
    }

    /**
     * @param string $meetingId
     * @param array $parameters
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createPoll(string $meetingId, array $parameters)
    {
        return $this->client->request('POST', 'meetings/' . $meetingId . '/polls', [
            'headers' => $this->headers,
            'json' => $parameters
        ]);
    }

    /**
     * @param string $meetingId
     * @param string $pollId
     * @param array $parameters
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updatePoll(string $meetingId, string $pollId, array $parameters)
    {
        return $this->client->request('PATCH', 'meetings/' . $meetingId . '/polls/' . $pollId, [
            'headers' => $this->headers,
            'json' => $parameters
        ]);
    }

    /**
     * @param string $meetingId
     * @param string $pollId
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function deletePoll(string $meetingId, string $pollId)
    {
        return $this->client->request('DELETE', 'meetings/' . $meetingId . '/polls/' . $pollId, [
            'headers' => $this->headers
        ]);
    }

    /**
     * @param string $meetingId
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getPollResults(string $meetingId)
    {
        return $this->client->request('GET', 'meetings/' . $meetingId . '/polls/results', [
            'headers' => $this->headers
        ]);
    }

    /**
     * @param string $meetingId
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function startLivestreaming(string $meetingId)
    {
        return $this->client->request('PUT', 'meetings/' . $meetingId . '/livestreaming/start', [
            'headers' => $this->headers
        ]);
    }

    /**
     * @param string $meetingId
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function stopLivestreaming(string $meetingId)
    {
        return $this->client->request('PUT', 'meetings/' . $meetingId . '/livestreaming/stop', [
            'headers' => $this->headers
        ]);
    }
}